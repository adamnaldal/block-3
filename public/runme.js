// based on the mazerunenr demo of RoverCam for p5 js:
//    Engineer: jWilliamDunn 2020
//    Ported to JS from github.com/jrc03c/queasycam/tree/master/examples/MazeRunner

let blockSize = 1;
let roomSize = 97;

class Block {
  constructor(x, y, z, w, h, d) {
    this.position = createVector(x, y, z);
    this.dimensions = createVector(w, h, d);
    this.fillColor = color(random(150, 200));
    this.visited = false;
  }

  update() {
    let boxLeft = this.position.x;
    let boxRight = this.position.x + this.dimensions.x;
    let boxTop = this.position.y + this.dimensions.y;
    let boxBottom = this.position.y;
    let boxFront = this.position.z;
    let boxBack = this.position.z + this.dimensions.z;

    let playerLeft = player.position.x - player.dimensions.x / 2;
    let playerRight = player.position.x + player.dimensions.x / 2;
    let playerTop = player.position.y - player.dimensions.y * 0.1;
    let playerBottom = player.position.y + player.dimensions.y * 0.9;
    let playerFront = player.position.z - player.dimensions.z / 2;
    let playerBack = player.position.z + player.dimensions.z / 2;

    let nextPlayerLeft = playerRight + player.velocity.x;
    let nextPlayerRight = playerLeft + player.velocity.x;
    let nextPlayerTop = playerTop + player.velocity.y;
    let nextPlayerBottom = playerBottom + player.velocity.y;
    let nextPlayerFront = playerFront + player.velocity.z;
    let nextPlayerBack = playerBack + player.velocity.z;

    if (playerRight > boxLeft &&
      playerLeft < boxRight &&
      playerBack > boxFront &&
      playerFront < boxBack &&
      nextPlayerTop < boxBottom &&
      nextPlayerBottom > boxTop){
        //console.log("y collision")
        player.velocity.y = 0;
        player.position.y = (boxTop - 0.02) - (player.dimensions.y * 0.9)

        playerTop = player.position.y - player.dimensions.y * 0.1;
        playerBottom = player.position.y + player.dimensions.y * 0.9;
      }

    if (nextPlayerRight > boxLeft &&
      nextPlayerLeft < boxRight &&
      playerBack > boxFront &&
      playerFront < boxBack &&
      playerTop < boxBottom &&
      playerBottom > boxTop){
        //console.log("x collision");
        player.velocity.x = 0;

        nextPlayerLeft = playerRight + player.velocity.x;
        nextPlayerRight = playerLeft + player.velocity.x;
      }

    if (playerRight > boxLeft &&
      playerLeft < boxRight &&
      nextPlayerBack > boxFront &&
      nextPlayerFront < boxBack &&
      playerTop < boxBottom &&
      playerBottom > boxTop){
        //console.log("z collision");
        player.velocity.z = 0;

        playerFront = player.position.z - player.dimensions.z / 2;
        playerBack = player.position.z + player.dimensions.z / 2;
      }



  }

  display() {
    push();
    translate(this.position.x+(this.dimensions.x/2), this.position.y+(this.dimensions.y/2), this.position.z+(this.dimensions.z/2));
    fill(this.fillColor);
    box(this.dimensions.x, this.dimensions.y, this.dimensions.z);
    pop();
  }

  pushDown() {
    this.position.y += blockSize;
  }
}

class Maze {
  constructor(size) {
    this.blocks = [
      new Block(0, blockSize, 0,                  size, -blockSize, size), //floor
      //boys room
      new Block(-blockSize,0,0,           blockSize,-20,44), //wall along Z
      new Block(0,0,-blockSize,           44,-20,blockSize), // wall along x
      new Block(44,0,0,                   blockSize,-20,44-5.5), // wall along z inside room
      new Block(0,0,44,                   44-5.5,-20,blockSize), // wall along x inside room
      // unisex room
      new Block(44-5.5-blockSize,0,44-4,  blockSize,-1.2,10), //wall along Z
      new Block(44-4,0,44-5.5-blockSize,  10,-1.2,blockSize), // wall along x
      new Block(55-4-blockSize,0,44-5.5,  blockSize,-1.2,10), // wall along z inside room
      new Block(44-5.5,0,55-4-blockSize,  10,-1.2,blockSize), // wall along x inside room
      // girls room
      new Block(44,0,44+blockSize+5,           blockSize,-20,50-5.5), //wall along Z
      new Block(44+5+blockSize,0,44,           50-5.5,-20,blockSize), // wall along x
      new Block(44+50+0.5,0,44+blockSize,      blockSize,-20,50), // wall along z inside room
      new Block(44+blockSize,0,44+50+0.5,      50,-20,blockSize), // wall along x inside room
    ];


    // for (let i = 0; i < size; i++) {
    //   this.blocks[i] = new Array(size);
    //   for (let j = 0; j < size; j++) {
    //     let x = i * blockSize;
    //     let y = 0;
    //     let z = j * blockSize;
    //     this.blocks[i][j] = new Block(x, y, z, blockSize, blockSize, blockSize);
    //   }
    // }
    //
     this.start = this.blocks[0];
    // this.blocks[1][1].fillColor = color(63, 127, 63);
    //
    // for (let i = 1; i < size - 1; i++)
    //   for (let j = 1; j < size - 1; j++)
    //     if (m[i][j]) this.blocks[i][j].pushDown();
    //     else this.blocks[i][j].fillColor = color(127);
    // this.blocks[3][3].fillColor = color(127, 63, 63);
    for (let i = 0; i < this.blocks.length; i++){
      if (i == 0){
        this.blocks[i].fillColor = color(127);
        if (frameCount < 4){
          console.log(this.blocks[i].position);
        }
      } else if (i > 0 && i < 5) {
        this.blocks[i].fillColor = color(20, 63, 150);
      } else if (i > 4 && i < 9) {
        this.blocks[i].fillColor = color(127, 63, 127);
      } else if (i > 8 && i < 13) {
        this.blocks[i].fillColor = color(180, 50, 79);
      }
    }
  }

  update() {
    for (let i = 0; i < this.blocks.length; i++) {

        this.blocks[i].update();
        // if (i==4){
        //   console.log(i, this.blocks[i].update());
        // }

    }
  }

  display() {
    for (let i = 0; i < this.blocks.length; i++) {

        this.blocks[i].display();

    }
  }

  setPlayerAtStart(player) {
    player.reset();
    player.position = p5.Vector.add(this.start.position, createVector(2, -20, 2));
  }
}


class Player extends RoverCam {
  constructor(maze) {
    super();
    this.dimensions = createVector(1, 1.8, 1);   // min elevation = 0.2
    this.velocity = createVector(0, 0, 0);
    this.gravity = createVector(0, 0.04, 0);
    this.grounded = false;
    this.speed = 0.02;
    this.sensitivity = 0.02;
    console.log(maze.blocks);
  }

  update() {
    // ffd8 - add SHIFT for running!
    this.speed = 0.008;
    this.sensitivity = 0.02;
    if (keyIsDown(16)) {
      this.sensitivity = 0.04;
      this.speed = 0.02;
    }
    if (keyIsPressed && key == 'e') {
      this.grounded = false;
      return;
    }

    if (this.grounded && keyIsDown(32)) { // space
      this.grounded = false;
      this.velocity.y = -1.5;
      this.position.y -= 0.2;
    }
  }

  move(){
    this.velocity.add(this.gravity);
    this.position.add(this.velocity);
  }
}

var player, maze, f, help = false;

function preload() {
  f = loadFont('inconsolata.otf');
}

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  strokeWeight(0.04);
  textFont(f);
  textSize(10);
  maze = new Maze(roomSize);
  player = new Player(maze);
  player.usePointerLock();
  maze.setPlayerAtStart(player);
  player.yaw(PI/4);
  frameRate(60);
  strokeWeight(2);
}

function keyPressed() {
  if (key == 'h') help = !help;
  // ffd8 - respawn with DELETE or BACKSPACE
  if (keyCode == 8 || keyCode == 46) {
    maze.setPlayerAtStart(player);
  }
}

function windowResized() {
  resizeCanvas(window.innerWidth, window.innerHeight, WEBGL);
}

function draw() {
  background(0, 0, 51);
  lights(255);
  directionalLight(250, 230, 230, 50, -50, 20)

  player.update();
  maze.update();
  maze.display();
  player.move();
  //drawAxes();

  if (help || frameCount < 400) { // Heads Up Display extension by jWilliam
    push(); // this affects the frame rate
    camera(0, 0, (height / 2.0) / tan(PI * 30.0 / 180.0), 0, 0, 0, 0, 1, 0);
    ortho(-width / 2, width / 2, -height / 2, height / 2, 0, 1000);
    fill(0, 0, 0, 200);
    noStroke();
    translate(-380, -150, 0);
    scale(2);
    rect(0, 0, 140, 85);
    fill(127);
    text('mouse: left/right : pan', 10, 10);
    text('       up/down : tilt', 10, 20);
    text('       click : ptrlock', 10, 30);
    text(' keys: a/d : left/right', 10, 40);
    text('       w/s : fwd/bkwd', 10, 50);
    text('       e/q : up/down', 10, 60);
    text('       space : jump', 10, 70);
    text('       position: ' + player.position, 10, 80);
    pop();
  }
}

function drawAxes(){
	push();
      noStroke();
	  fill(127,0,0); // X red
	  translate(75,0.5,0.5);
	  box(150,1,1);
	pop();
	push();
      noStroke();
	  fill(0,127,0); // Y green
	  translate(0.5,75,0.5);
	  box(1,150,1);
	pop();
	push();
      noStroke();
	  fill(0,0,127); // Z blue
	  translate(0.5,0.5,75);
	  box(1,1,150);
	pop();
}
